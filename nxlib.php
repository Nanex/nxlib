<?php

/*
 * The MIT License
 *
 * Copyright 2014 Nanex.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @license <http://opensource.org/licenses/MIT> The MIT License (MIT)
 * @author Nanex <nanex@greyfire.de>
 * @version 0.1
 * Classloader for NxLib
 */
class nxlib {

  private $classes = array("page", "user");
  private $static = array("str", "sql", "css", "nCrypt", "nException", "nLog", "user");
  private $dir = "src/";
  private $phpExt = ".php";

  /**
   * @since 0.1
   * Initializes the object
   */
  public function __construct($loadStatics = TRUE) {
    if ($loadStatics) {
      $this->loadStatics();
    }
  }

  /**
   * @since 0.1
   * Requires all nxlib files with static classes
   */
  public function loadStatics() {
    foreach ($this->static as $static) {
      require_once $this->dir . $static . $this->phpExt;
    }
    require_once 'misc.php';
  }

  /**
   * @since 0.1
   * @param String $name Name of class to load
   * @param String $arguments Arguments for class to load
   * @return object Object of class to load
   * Loads files & classes of nxlib when needed
   */
  public function __call($name, $arguments) {
    $arguments = implode(",", $arguments);
    if (in_array($name, $this->classes)) {
      require_once $this->dir . $name . $this->phpExt;
      return eval("return new nxlib\\" . $name . "(" . $arguments . ");");
    } elseif (in_array($name, $this->static)) {
      require_once $this->dir . $name . $this->phpExt;
      return eval("return new nxlib\\" . $name . "(" . $arguments . ");");
    }
  }

}
