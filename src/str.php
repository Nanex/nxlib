<?php

/*
 * The MIT License
 *
 * Copyright 2014 Nanex.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace nxlib;

/**
 * @license <http://opensource.org/licenses/MIT> The MIT License (MIT)
 * @author Nanex <nanex@greyfire.de>
 * @internal Only static functions here!
 * @package nxlib
 * @version 0.1
 * General string functions
 */
class str {

  /**
   * @static
   * @since 0.1
   * @uses sql Needs sql.php for escaping strings with mysqli_real_escape_string
   * @param String $string string to escape
   * @return String $string escaped string
   * Escapes string
   */
  public static function esc($string) {
    if (get_magic_quotes_gpc()) {
      $string = stripslashes($string);
    }
    return mysqli_real_escape_string(sql::get(), $string);
  }

  /**
   * @static
   * @since 0.1
   * @param String $input string to hash
   * @return String $output 32 character long hash
   * md5-hashes every character and the resulting string
   */
  public static function md6($input) {
    $output = "";
    $length = strlen($input);
    for ($i = 0; $i < $length; $i++) {
      $output .= md5($input[$i]);
    }
    return md5($output);
  }
  
  public static function fod($dir){
    $dir = explode("/", $dir);
    if(!is_array($dir)){
      $dir = explode("\\", $dir);
    }else{
      $dir = explode("\\", $dir[0]);
    }
    return end($dir);
  }

}
