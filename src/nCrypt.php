<?php

/*
 * The MIT License
 *
 * Copyright 2014 Nanex.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace nxlib;

/**
 * @license <http://opensource.org/licenses/MIT> The MIT License (MIT)
 * @author Nanex <nanex@greyfire.de>
 * @package nxlib
 * @version 0.1
 * XOR encryption with use of base64 to prevent dataloss
 */
class nCrypt {

  /**
   * @since 0.1
   * @param String $string String to encrypt
   * @param String $key Key/Password to encrypt string with
   * @return String Encrypted string
   * Encrypts string with key and base64_encodes it
   */
  public static function enc($string, $key) {
    $string = md5($string) . $string;
    return base64_encode(self::crypt($string, $key));
  }

  /**
   * @since 0.1
   * @param String $string String to decrypt
   * @param String $key Key/Password to decrypt string with
   * @return String Decrypted string
   * Base64_decodes string and decrypts it
   */
  public static function dec($string, $key) {
    $string = self::crypt(base64_decode($string), $key);
    $hash = substr($string, 0, 32);
    $res = substr($string, 32);
    if ($hash === substr(md5($res), 0, 32)) {
      return $res;
    } else {
      return FALSE;
    }
  }

  /**
   * @since 0.1
   * @internal The actual XOR encryption/decryption
   */
  protected static function crypt($string, $key) {
    $output = array();
    $key = str_split(self::setKeyLength($string, $key), 1);
    $string = str_split($string, 1);
    for ($i = 0; $i < count($key); $i++) {
      array_push($output, chr(ord($string[$i]) ^ ord($key[$i])));
    }
    return implode("", $output);
  }

  /**
   * @since 0.1
   * @internal Repeating key to match length of string
   */
  protected static function setKeyLength($string, $key) {
    while (strlen($string) > strlen($key)) {
      $key .= $key;
    }
    while (strlen($string) < strlen($key)) {
      $key = substr($key, 0, strlen($key) - 1);
    }
    return $key;
  }

}
