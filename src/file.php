<?php

/**
 * Safe rewriting of files
 * @author Arno Groll <agroll@inconso.de>
 * @version 0.1
 */
class File {

  private static $maxWaitTime = 1000;
  
  public static function write($fp, $content, $mode = "a") {
    try {
      $fp = self::getHandle($fp, $mode);
      self::quickLock($fp);
    } catch (Exception $e) {
      throw $e;
    }
    fwrite($fp, $content);
    flock($fp, LOCK_UN);
    fclose($fp);
  }

  public static function read($fp, $array = false) {
    try {
      $fp = self::getHandle($fp, "r");
      self::quickLock($fp);
    } catch (Exception $e) {
      throw $e;
    }
    $content = fread($fp, fstat($fp)['size']);
    if ($array) {
      $content = explode("\n", $content);
    }
    flock($fp, LOCK_UN);
    fclose($fp);
    return $content;
  }

  public static function lock($fp, $mode = "w") {
    $fp = self::getHandle($fp, $mode);
    flock($fp, LOCK_EX);
    return $fp;
  }

  public static function unlock($fp) {
    $fp = self::getHandle($fp);
    flock($fp, LOCK_UN);
  }

  private static function quickLock($fp) {
    $i = 0;
    while (!flock($fp, LOCK_EX | LOCK_NB)) {
      $waitTime = rand(10, 100);
      usleep($waitTime);
      $i += $waitTime;
      if ($i >= self::$maxWaitTime) {
        throw new Exception("File system error!", 578);
      }
    }
  }

  private static function getHandle($fp, $mode = "a") {
    if (is_resource($fp)) {
      if (get_resource_type($fp) === 'file') {
        return $fp;
      }else{
        throw new Exception("Resource not found.", 414);
      }
    }
    if (is_file($fp)) {
      return fopen($fp, $mode);
    }
    if(touch($fp)){
      return fopen($fp, $mode);
    }
    throw new Exception("File not found!", 244);
  }

}
