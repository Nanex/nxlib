<?php

/**
 * Handles GET-Input
 * @author Arno Groll <agroll@inconso.de>
 * @version 0.1
 */
class Get {

  /**
   * Checks if GET is set
   */
  private static function start() {
    if ($_SERVER['REQUEST_METHOD'] === "GET") {
      return true;
    }
    return false;
  }

  /**
   * Returns GET as array (empty arry if $_GET does not exist)
   * @return array Whole GET-array
   */
  public static function getAll() {
    if (self::start()) {
      return $_GET;
    }
    return array();
  }

  /**
   * Returns value of GET-field at position of provided key (Null if no value found) and gives the option to sanitize the value
   * @param mixed $field Name of key
   * @return mixed Value of field
   */
  public static function getField($field, $filter = false) {
    if (self::start()) {
      if ($filter === false) {
        return filter_input(INPUT_GET, $field, $filter);
      }
      return $_GET[$field];
    }
    return null;
  }

  /**
   * Checks if Field is set
   * @param mixed $field Field-key
   * @return boolean True if field is set, false if $_GET or array-field not set
   */
  public static function fieldExists($field) {
    if (self::start()) {
      if (isset($_GET[$field])) {
        return true;
      }
    }
    return false;
  }

  /**
   * Compares Value in GET-field with variable
   * @param mixed $field Field of get-array to compare to variable
   * @param mixed $value Variable to compare to get-value
   * @return boolean True if field-value equals variable (also checks variable type)
   */
  public static function fieldCompare($field, $value) {
    if (self::fieldExists($field)) {
      if (self::getField($field) === $value) {
        return true;
      }
    }
    return false;
  }

}
