<?php

/**
 * Handles User-Session
 * @author Arno Groll <agroll@inconso.de>
 * @version 0.1
 */
class Session {

  /**
   * Starts session if not already started
   */
  private static function start() {
    if (session_status() == PHP_SESSION_NONE) {
      session_start();
    }
  }

  /**
   * Ends user-session
   * @return boolean True if session was active, false if it wasn't
   */
  public static function kill() {
    if (session_status() == PHP_SESSION_NONE) {
      session_destroy();
      return true;
    }
    return false;
  }

  /**
   * Replaces session with new array
   * @param array Array to replace Session
   * @throws Exception
   */
  public static function set($array) {
    self::start();

    if (is_array($array)) {
      $_SESSION = $array;
    } else {
      throw new Exception("Could not set Session!", 98);
    }
  }

  /**
   * Returns session as array
   * @return array Whole session-array
   */
  public static function get() {
    self::start();

    return $_SESSION;
  }

  /**
   * Sets field in session array
   * @param mixed $field Name of Key
   * @param mixed $value Value to be written in session-field
   * @pa
   */
  public static function setField($field, $value) {
    self::start();

    $_SESSION[$field] = $value;
  }

  /**
   * Returns value of session-field at position of provided key
   * @param mixed $field Name of Key
   * @return mixed Value of field
   * @throws Exception
   */
  public static function getField($field) {
    self::start();

    if (isset($_SESSION[$field])) {
      return $_SESSION[$field];
    } else {
      throw new Exception("Query on empty session-field!", 97);
    }
  }

  /**
   * Checks if Field is set
   * @param mixed $field Field-key
   * @return boolean True if field is set, false if it isn't
   */
  public static function fieldExists($field) {
    self::start();

    if (isset($_SESSION[$field])) {
      return true;
    }
    return false;
  }

  /**
   * Compares Value in Sessionfield with variable
   * @param type $field
   * @param type $value
   * @return boolean True if 
   */
  public static function fieldCompare($field, $value) {
    self::start();

    if (self::fieldExists($field)) {
      if (self::getField($field) === $value) {
        return true;
      }
    }
    return false;
  }

}
