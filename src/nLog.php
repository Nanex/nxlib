<?php

/*
 * The MIT License
 *
 * Copyright 2014 agroll.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace nxlib;

/**
 * @license <http://opensource.org/licenses/MIT> The MIT License (MIT)
 * @author Nanex <nanex@greyfire.de>
 * @package nxlib
 * @version 0.1
 * Logging script. Expects Message-String or Exception-Object
 */
class nLog {

  private static $disabled = FALSE;
  private static $initialized = FALSE;
  private static $logfile;
  private static $time;
  private static $ip;
  private static $template = "#date#    #ip#    #message#";
  private static $temp;

  private static function init() {
    if (!self::$initialized) {
      self::$time = date("Y-m-d | H:i:s");
      self::$ip = str_pad($_SERVER['REMOTE_ADDR'], 15);
      self::$logfile = "../log/" . date("Y-m-d") . ".log";
    }
    self::$temp = str_replace("#ip#", self::$ip, str_replace("#date#", self::$time, self::$template));
    self::$initialized = TRUE;
  }

  public static function add($input) {
    if (self::$disabled) {
      return FALSE;
    }
    if (!self::$initialized) {
      self::init();
    }
    if (of_class($input, "Exception")) {
      $message = "EXCEPTION " . $input->getCode() . " | " . $input->getMessage() . " | File: " . str::fod($input->getFile()) . " | Line: " . $input->getLine();
    } else {
      $message = $input;
    }
    self::writeLog($message);
  }

  private static function writeLog($message) {
    $output = str_replace("#message#", $message, self::$temp) . "\n";
    $dir = getcwd();
    chdir(__DIR__);
    try {
      if (!file_put_contents(self::$logfile, $output, FILE_APPEND)) {
        throw new \Exception("Could not write to file.");
      }
    } catch (\Exception $e) {
      self::$disabled = TRUE;
      echo "Log disabled: " . $e->getMessage() . " | " . time();
    } finally {
      chdir($dir);
    }
  }

  public static function setTemplate($new) {
    if (!empty($new) && is_string($new)) {
      self::$template = $new;
      self::init();
    } else {
      throw new nException("string expected, " . gettype($new) . " received.");
    }
  }

  public static function getTemplate() {
    return self::$template;
  }

}
