<?php

/*
 * The MIT License
 *
 * Copyright 2014 Nanex.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace nxlib;

/**
 * @license <http://opensource.org/licenses/MIT> The MIT License (MIT)
 * @author Nanex <nanex@greyfire.de>
 * @package nxlib
 * @version 0.1
 * Connects to mysql database and saves mySQLi-object
 */
class sql {

  const host = "localhost";
  const username = "root";
  const password = "toor";
  const database = "test";

  protected static $connection = FALSE;

  /**
   * @since 0.1
   * Connects to database and saves object if not already saved
   */
  protected static function connect($database = FALSE) {
    if ($database === FALSE) {
      $database = self::database;
    }
    if (self::$connection === FALSE || $database !== FALSE) {
      self::$connection = new mysqli(self::host, self::username, self::password, $database);
      if (self::$connection->connect_errno) {
        die("No database connection established!");
      }
    }
  }

  /**
   * @since 0.1
   * @return object MySQLi-object
   * Returns MySQLi-object
   */
  public static function get($database = FALSE) {
    self::connect($database);
    return self::$connection;
  }

}
